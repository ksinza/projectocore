﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace ProjectCore.Logica.BL
{
    public class Tasks
    { 
        
        /// <summary>
    /// GEt task 
    /// </summary>
    /// <param name="projectId"></param>
    /// <param name="id"></param>
    /// <returns></returns>
        public List<Models.DB.Tasks> GetTasks(int? projectId,
            int? id)
        {
            DAL.Models.ProjectCoreContext _context = new DAL.Models.ProjectCoreContext();
            var listTask = (from _task in _context.Tasks
                            join _states in _context.States on _task.StateId equals _states.Id
                            join _activities in _context.Activities on _task.ActivityId equals _activities.Id
                            join _priorities in _context.Priorities on _task.PriorityId equals _priorities.Id
                            select new Models.DB.Tasks
                            {
                                Id = _task.Id,
                                Title = _task.Title,
                                Details = _task.Details,
                                ExpirationDate = _task.ExpirationDate,
                                IsCompleted = _task.IsCompleted,
                                Effort = _task.Effort,
                                RemainingWork = _task.RemainingWork,
                                StateId = _task.StateId,
                                States = new Models.DB.States
                                {
                                    Name= _states.Name
                                },
                                PriorityId = _task.ProjectId,
                                Priorities = new Models.DB.Priorities
                                {
                                    Name= _priorities.Name
                                },
                                ActivityId = _task.ActivityId,
                                Activities = new Models.DB.Activities
                                {
                                    Name = _activities.Name

                                },
                                ProjectId = _task.PriorityId

                            }).ToList();
            if (projectId != null)
            {
                listTask = listTask.Where(x => x.PriorityId == projectId).ToList();
            }
            if (id !=null)
            {
                listTask = listTask.Where(x => x.Id ==id).ToList();

            }
            return listTask;
        }
        /// <summary>
        /// create tasks
        /// </summary>
        /// <param name="title"></param>
        /// <param name="details"></param>
        /// <param name="expirationDate"></param>
        /// <param name="isCompleted"></param>
        /// <param name="effort"></param>
        /// <param name="remainingwork"></param>
        /// <param name="stateId"></param>
        /// <param name="activityId"></param>
        /// <param name="priorityId"></param>
        /// <param name="projectId"></param>
        public void CreateTasks(string title,
            string details, 
            DateTime? expirationDate,
            bool? isCompleted,
            int? effort,
            int? remainingwork,
            int? stateId,
            int? activityId,
            int? priorityId,
            int? projectId)
        {
            DAL.Models.ProjectCoreContext _context = new DAL.Models.ProjectCoreContext();
            _context.Tasks.Add( new DAL.Models.Tasks { 
            
                Title = title,
                Details = details,
                ExpirationDate= expirationDate,
                IsCompleted= isCompleted,
                Effort= effort,
                RemainingWork= remainingwork,
                StateId = stateId,
                ActivityId= activityId,
                PriorityId= priorityId,
                ProjectId= projectId
            
            });

            _context.SaveChanges();


        }
    }
}
