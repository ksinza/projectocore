﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace ProjectCore.Logica.Models.BindingModel
{
    public class TasksCreateBindingModel
    {
        [Display(Name = "Title")]
        [Required(ErrorMessage = "This field Title is required")]
        public string Title { get; set; }

        [Display(Name = "Details")]
        [Required(ErrorMessage = "This field Details is required")]
        public string Details { get; set; }

        [Display(Name = "ExpirationDate")]
        [Required(ErrorMessage = "This field ExpirationDate is required")]
        public DateTime? ExpirationDate { get; set; }

        [Display(Name = "ExpirationDate")]
        [Required(ErrorMessage = "This field is required")]
        public bool IsCompleted { get; set; }

        [Display(Name = "Effort")]
        [Required(ErrorMessage = "This field Effort is required")]
        public int? Effort { get; set; }

        [Display(Name = "RemainingWork")]
        [Required(ErrorMessage = "This field  RemainingWork is required")]
        public int? RemainingWork { get; set; }

        [Display(Name = "State")]
        [Required(ErrorMessage = "This field State is required")]
        public int? StateId { get; set; }

        [Display(Name = "Activity")]
        [Required(ErrorMessage = "This field Activity is required")]
        public int? ActivityId { get; set; }

        [Display(Name = "Priority")]
        [Required(ErrorMessage = "This field Priority is required")]
        public int? PriorityId { get; set; }

        public int? ProjectId { get; set; }

    }
}
