﻿using System;
using System.Collections.Generic;

namespace ProjectCore.DAL.Models
{
    public partial class DocumentTypes
    {
        public DocumentTypes()
        {
            Customer = new HashSet<Customer>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool? Active { get; set; }

        public ICollection<Customer> Customer { get; set; }
    }
}
