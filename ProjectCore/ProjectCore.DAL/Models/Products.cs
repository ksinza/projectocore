﻿using System;
using System.Collections.Generic;

namespace ProjectCore.DAL.Models
{
    public partial class Products
    {
        public Products()
        {
            ProductPhotos = new HashSet<ProductPhotos>();
            SaleDetails = new HashSet<SaleDetails>();
        }

        public int Id { get; set; }
        public int? CategoryId { get; set; }
        public string Name { get; set; }
        public double? Price { get; set; }
        public double? ShippingCost { get; set; }
        public string Warranty { get; set; }
        public string Description { get; set; }
        public int? Quantity { get; set; }
        public int? StateId { get; set; }
        public int? CustomerId { get; set; }

        public Categories Category { get; set; }
        public Customer Customer { get; set; }
        public States State { get; set; }
        public ICollection<ProductPhotos> ProductPhotos { get; set; }
        public ICollection<SaleDetails> SaleDetails { get; set; }
    }
}
