﻿using System;
using System.Collections.Generic;

namespace ProjectCore.DAL.Models
{
    public partial class Customer
    {
        public Customer()
        {
            Products = new HashSet<Products>();
            Sales = new HashSet<Sales>();
        }

        public int Id { get; set; }
        public int? DocumentTypeId { get; set; }
        public string DocumentNumber { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Surname { get; set; }
        public string SecondSurname { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public int? CityId { get; set; }
        public string UserId { get; set; }

        public Cities City { get; set; }
        public DocumentTypes DocumentType { get; set; }
        public ICollection<Products> Products { get; set; }
        public ICollection<Sales> Sales { get; set; }
    }
}
