﻿using System;
using System.Collections.Generic;

namespace ProjectCore.DAL.Models
{
    public partial class ProductPhotos
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public string Guid { get; set; }

        public Products Product { get; set; }
    }
}
