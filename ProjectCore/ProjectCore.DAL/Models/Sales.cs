﻿using System;
using System.Collections.Generic;

namespace ProjectCore.DAL.Models
{
    public partial class Sales
    {
        public Sales()
        {
            SaleDetails = new HashSet<SaleDetails>();
        }

        public int Id { get; set; }
        public int? MethodPaymentId { get; set; }
        public double? TotalValue { get; set; }
        public DateTime? Date { get; set; }
        public int? CustomerId { get; set; }

        public Customer Customer { get; set; }
        public MethodPayment MethodPayment { get; set; }
        public ICollection<SaleDetails> SaleDetails { get; set; }
    }
}
