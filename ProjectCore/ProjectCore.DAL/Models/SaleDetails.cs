﻿using System;
using System.Collections.Generic;

namespace ProjectCore.DAL.Models
{
    public partial class SaleDetails
    {
        public int Id { get; set; }
        public int? SaleId { get; set; }
        public int? ProductId { get; set; }
        public int? Quantity { get; set; }
        public double? SubtotalValue { get; set; }

        public Products Product { get; set; }
        public Sales Sale { get; set; }
    }
}
