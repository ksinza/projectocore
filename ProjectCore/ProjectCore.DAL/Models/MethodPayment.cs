﻿using System;
using System.Collections.Generic;

namespace ProjectCore.DAL.Models
{
    public partial class MethodPayment
    {
        public MethodPayment()
        {
            Sales = new HashSet<Sales>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool? Active { get; set; }

        public ICollection<Sales> Sales { get; set; }
    }
}
