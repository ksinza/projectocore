﻿using System;
using System.Collections.Generic;

namespace ProjectCore.DAL.Models
{
    public partial class Categories
    {
        public Categories()
        {
            Products = new HashSet<Products>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool? Active { get; set; }

        public ICollection<Products> Products { get; set; }
    }
}
